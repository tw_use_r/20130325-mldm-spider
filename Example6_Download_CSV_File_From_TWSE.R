
rm(list=ls(all.names=TRUE))
library(RCurl)
library(XML)

test = getURL("http://www.twse.com.tw/en/trading/exchange/MI_INDEX/MI_INDEX3_print.php?genpage=genpage/Report201211/A11220121115ALLBUT0999_1.php&type=csv")
test = getURL("http://www.twse.com.tw/en/trading/exchange/MI_INDEX/MI_INDEX3_print.php?genpage=genpage/Report201305/A11220130503ALL_1.php&type=csv")
fileConn<-file("output.csv")
writeLines(test, fileConn)
close(fileConn)

fileConn<-file("output.csv")
test_Ls = readLines(fileConn)
close(fileConn)
View(test_Ls)


one_line_data <- read.table(textConnection(test_Ls[973]), sep = ",")
View(one_line_data)
one_line_data <- read.table(textConnection(test_Ls[9088]), sep = ",")
View(one_line_data)

Total_Data_Temp = lapply(test_Ls,function(one_line_data){
  try(read.table(textConnection(one_line_data), sep = ",",stringsAsFactors=FALSE))
})
Has_Data_Index = which(sapply(Total_Data_Temp,length) == 15)
Total_Data = Total_Data_Temp[Has_Data_Index]

Total_Data_DF = do.call(rbind,Total_Data[-1])

colnames(Total_Data_DF) = c("StockID","Volume","Transaction","TradeValue","Open","High","Low","Close","Dir","Change","BestBidPrice","BestBidVolume","BestAskPrice","BestAskVolume","PER")
Total_Data_DF = Total_Data_DF[,-9]
View(Total_Data_DF)

Total_Data_DF$StockID = gsub("=","",Total_Data_DF$StockID)
Total_Data_DF$Volume = as.integer(gsub(",","",Total_Data_DF$Volume))
Total_Data_DF$Transaction = as.integer(sub(",","",Total_Data_DF$Transaction))
Total_Data_DF$TradeValue = as.double(gsub(",","",Total_Data_DF$TradeValue))
Total_Data_DF$Open = as.double(gsub(",","",gsub("--",NA,Total_Data_DF$Open)))
Total_Data_DF$High = as.double(gsub(",","",gsub("--",NA,Total_Data_DF$High)))
Total_Data_DF$Low = as.double(gsub(",","",gsub("--",NA,Total_Data_DF$Low)))
Total_Data_DF$Close = as.double(gsub(",","",gsub("--",NA,Total_Data_DF$Close)))

Total_Data_DF$BestBidPrice = as.double(gsub(",","",gsub("--",NA,Total_Data_DF$BestBidPrice)))
Total_Data_DF$BestAskPrice = as.double(gsub(",","",gsub("--",NA,Total_Data_DF$BestAskPrice)))

Total_Data_DF$BestBidVolume = as.integer(sub(",","",Total_Data_DF$BestBidVolume))
Total_Data_DF$BestAskVolume = as.integer(sub(",","",Total_Data_DF$BestAskVolume))

Total_Data_DF$PER = as.double(gsub(",","",gsub("--",NA,Total_Data_DF$PER)))

Date_Str = unlist(strsplit(test_Ls[[1]],split=" "))[1]

Total_Data_DF = data.frame(Date=Date_Str,Total_Data_DF)

View(Total_Data_DF)

